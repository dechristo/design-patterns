﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Strategy.Tests
{
    [TestClass]
    public class ReloadStrategyTest
    {
        [TestMethod]
        public void TestReloadStrategyImplementsAssaultRifleStrategy()
        {
            ReloadStrategy rs = new ReloadStrategy();
            Assert.IsInstanceOfType(rs, typeof(AssaultRifleStrategy));
        }

        [TestMethod]
        public void TestReloadStrategyActionReturnsCorrectAction()
        {
            ReloadStrategy rs = new ReloadStrategy();
            string action = rs.Action();
            Assert.AreEqual(action, "Reloading assault rifle...\nAssault rifle reloaded!");
        }
    }
}
