﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Strategy.Tests
{
    [TestClass]
    public class FireGranadeStrategyTest
    {
        [TestMethod]
        public void TestFireGranadeStrategyImplementsAssaultRifleStrategy()
        {
            FireGranadeStrategy fgs = new FireGranadeStrategy();
            Assert.IsInstanceOfType(fgs, typeof(AssaultRifleStrategy));
        }

        [TestMethod]
        public void TestFireGranadeStrategyActionReturnsCorrectAction()
        {
            FireGranadeStrategy fgs = new FireGranadeStrategy();
            string action = fgs.Action();
            Assert.AreEqual(action, "BUMPF! Granade launched!!");
        }
    }
}
