﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Strategy;

namespace Strategy.Tests
{
    [TestClass]
    public class AssaultRifleContextTest
    {
        [TestMethod]
        public void TestConstructorSetsConcreteStrategyForFireStrategy()
        {
            AssaultRifleContext arc = new AssaultRifleContext(new FireStategy());
            Assert.AreEqual(arc.Action(), "Firing!!!!");
        }

        [TestMethod]
        public void TestConstructorSetsConcreteStrategyForFireGranadeStrategy()
        {
            AssaultRifleContext arc = new AssaultRifleContext(new FireGranadeStrategy());
            Assert.AreEqual(arc.Action(), "BUMPF! Granade launched!!");
        }

        [TestMethod]
        public void TestConstructorSetsConcreteStrategyForReloadStrategy()
        {
            AssaultRifleContext arc = new AssaultRifleContext(new ReloadStrategy());
            Assert.AreEqual(arc.Action(), "Reloading assault rifle...\nAssault rifle reloaded!");
        }
    }
}
