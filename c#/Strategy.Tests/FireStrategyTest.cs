using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Strategy.Tests
{
    [TestClass]
    public class FireStrategyTest
    {
        [TestMethod]
        public void TestFireStrategyImplementsAssaultRifleStrategy()
        {
            FireStategy fs = new FireStategy();
            Assert.IsInstanceOfType(fs, typeof(AssaultRifleStrategy));
        }

        [TestMethod]
        public void TestFireStrategyActionReturnsCorrectAction()
        {
            FireStategy fs = new FireStategy();
            string action = fs.Action();
            Assert.AreEqual(action, "Firing!!!!");
        }
    }
}
