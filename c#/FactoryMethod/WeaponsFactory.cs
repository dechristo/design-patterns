﻿using System;

namespace FactoryMethod
{
    public class WeaponsFactory
    {
        //TODO: add param info which will be an object that contains information about model, type, manufacturer, etc)
        public static Weapon Create(EnumWeapons.Weapon weapon)
        {
            switch(weapon)
            {
                case EnumWeapons.Weapon.AssaultRifle:
                    return new AssaultRifle();    
                case EnumWeapons.Weapon.Pistol:
                    return new Pistol();
                default: 
                    throw new NotImplementedException("Weapon type not implemented!");
            }   
        }
    }
}
