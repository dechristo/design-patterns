﻿using System;

namespace FactoryMethod
{
    public class Pistol : Weapon
    {
        public Pistol(string model = "Unspecified", string ammo = "Unspecified") 
            : base(model, ammo) {}

        public override string Fire()
        {
            return String.Format("Firing with {0} pistol!", this.Model);
        }

        public override string Reload()
        {
            return String.Format(
                "Reloading {0} with {1} rounds...",this.Model, this.Ammo);
        }
    }
}
