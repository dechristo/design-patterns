﻿using System;

namespace FactoryMethod
{
    public class AssaultRifle : Weapon
    {
        public AssaultRifle(string model = "Unspecified", string ammo ="Unspecified")
            : base (model, ammo) {}

        public override string Fire()
        {
            return String.Format(
                "Firing {0} with {1} caliber shells", this.Model, this.Ammo);
        }

        public override string Reload()
        {
            return String.Format("Reloading {0} bullets clip...", this.Ammo);
        }
    }
}
