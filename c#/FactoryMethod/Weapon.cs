﻿using System;

namespace FactoryMethod
{
    public abstract class Weapon
    {
        private string _model;
        private string _ammo;

        public Weapon(string model, string ammo)
        {
            this._model = model;
            this._ammo = ammo;
        }

        public string Model 
        {
            get { return this._model; }
        }

        public string Ammo
        {
            get { return this._ammo; }
        }

        public string GetInfo()
        {
            return String.Format(
                "Model: {0}\nAmmo:{1}", this._model, this._ammo); 
        }

        public abstract string Fire();
        public abstract string Reload();
    }
}
