﻿using System;

namespace FactoryMethod
{
    public class EnumWeapons
    {
        public enum Weapon 
        {
            Pistol,
            AssaultRifle,
            Knife,
            Shotgun
        };
    }
}
