﻿using System;

namespace Strategy
{
    public class ReloadStrategy : AssaultRifleStrategy
    {
        public override string Action()
        {
            return "Reloading assault rifle...\nAssault rifle reloaded!";
        }
    }
}