﻿using System;

namespace Strategy
{
    public class AssaultRifleContext
    {
        private AssaultRifleStrategy _arStrategy;

        public AssaultRifleContext(AssaultRifleStrategy strategy)
        {
            this._arStrategy = strategy;
        }

        public string Action()
        {
            return this._arStrategy.Action();
        }
    }
}
