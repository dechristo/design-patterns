﻿using System;

namespace Strategy
{
    public class FireGranadeStrategy : AssaultRifleStrategy
    {
		public override string Action()
		{
            return "BUMPF! Granade launched!!";
		}
	}
}
