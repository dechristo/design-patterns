﻿using System;

namespace Strategy
{
    public abstract class AssaultRifleStrategy
    {
        public abstract string Action();
    }
}
