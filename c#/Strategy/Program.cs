﻿using System;

namespace Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            AssaultRifleContext context;

            context = new AssaultRifleContext(new FireStategy());
            Console.WriteLine(context.Action());
            Console.WriteLine(context.Action());

            context = new AssaultRifleContext(new ReloadStrategy());
            Console.WriteLine(context.Action());

            context = new AssaultRifleContext(new FireStategy());
            Console.WriteLine(context.Action());

            context = new AssaultRifleContext(new FireGranadeStrategy());
            Console.WriteLine(context.Action());
        }
    }
}
