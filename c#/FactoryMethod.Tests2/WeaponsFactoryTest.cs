﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FactoryMethod.Tests
{
    [TestClass]
    public class WeaponsFactoryTest
    {
        [TestMethod]
        public void TestFactoryReturnsCorrectInstanceForPistol()
        {
            Weapon w = WeaponsFactory.Create(EnumWeapons.Weapon.Pistol);
            Assert.IsInstanceOfType(w, typeof(Pistol));
        }

        [TestMethod]
        public void TestFactoryReturnsCorrectInstanceForAssaultRifle()
        {
            Weapon w = WeaponsFactory.Create(EnumWeapons.Weapon.AssaultRifle);
            Assert.IsInstanceOfType(w, typeof(AssaultRifle));
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void TestFactoryThrowsExceptionForUnimplementedWeaponType()
        {
            Weapon w = WeaponsFactory.Create(EnumWeapons.Weapon.Knife);
        }
    }
}
