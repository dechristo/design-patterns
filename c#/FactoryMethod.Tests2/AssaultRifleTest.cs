﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FactoryMethod.Tests
{
    [TestClass]
    public class AssaultRifleTest
    {
        [TestMethod]
        public void TestConstructorReturnsInstanceWithDefaultParameters()
        {
            AssaultRifle ar = new AssaultRifle();

            Assert.IsInstanceOfType(ar, typeof(Weapon));
            Assert.AreEqual(ar.Model, "Unspecified");
            Assert.AreEqual(ar.Ammo, "Unspecified");
        }

        [TestMethod]
        public void TestConstructorReturnsInstanceWithCustomParametersSet()
        {
            AssaultRifle ar = new AssaultRifle("M4A1", "5.56mm");

            Assert.IsInstanceOfType(ar, typeof(Weapon));
            Assert.AreEqual(ar.Model, "M4A1");
            Assert.AreEqual(ar.Ammo, "5.56mm");
        }

        [TestMethod]
        public void TestFireReturnsCorrectValue()
        {
            AssaultRifle ar = new AssaultRifle("M16", "5.56mm");
            Assert.AreEqual(ar.Fire(), "Firing M16 with 5.56mm caliber shells");
        }

        [TestMethod]
        public void TestReloadReturnsCorrectValue()
        {
            AssaultRifle ar = new AssaultRifle("AK47", "5.53mm");
            Assert.AreEqual(ar.Reload(), "Reloading 5.53mm bullets clip...");
        }
    }
}