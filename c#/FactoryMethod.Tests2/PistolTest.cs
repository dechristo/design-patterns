﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FactoryMethod.Tests
{
    [TestClass]
    public class PistolTest
    {
        [TestMethod]
        public void TestConstructorReturnsInstanceWithDefaultParameters()
        {
            Pistol p = new Pistol();

            Assert.IsInstanceOfType(p, typeof(Weapon));
			Assert.AreEqual(p.Model, "Unspecified");
            Assert.AreEqual(p.Ammo, "Unspecified");
        }

        [TestMethod]
        public void TestConstructorReturnsInstanceWithCustomParametersSet()
        {
            Pistol p = new Pistol("Glock", "9mm");

            Assert.IsInstanceOfType(p, typeof(Weapon));
            Assert.AreEqual(p.Model, "Glock");
            Assert.AreEqual(p.Ammo, "9mm");
        }

        [TestMethod]
        public void TestFireReturnsCorrectValue()
        {
            Pistol p = new Pistol("P320", ".40");

            Assert.AreEqual(p.Fire(), "Firing with P320 pistol!");
        }

        [TestMethod]
        public void TestReloadReturnsCorrectValue()
        {
            Pistol p = new Pistol("P320", ".40");

            Assert.AreEqual(p.Reload(),"Reloading P320 with .40 rounds...");
        }
    }
}
