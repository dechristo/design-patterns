from unittest import TestCase
from src.model.m16 import M16


class M16Test(TestCase):
    def setUp(self):
        self.assault_rifle = M16()

    def test_origin_is_USA(self):
        self.assertEqual('USA', self.assault_rifle.origin)

    def test_fire_rate_m16_returns_correct_values(self):
        self.assertEqual(40, self.assault_rifle.fire_rate[0])
        self.assertEqual(100, self.assault_rifle.fire_rate[1])

    def test_shoot_m16_returns_correct_message(self):
        self.assertEqual('Shooting 100 rounds per minute!!!', self.assault_rifle.shoot())
