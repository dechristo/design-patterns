from unittest import TestCase
from src.model.fnscar import FNSCAR


class FNSCARTest(TestCase):
    def setUp(self):
        self.assault_rifle = FNSCAR()

    def test_origin_is_Belgium(self):
        self.assertEqual('Belgium', self.assault_rifle.origin)

    def test_fire_rate_for_fnscar_returns_correct_values(self):
        self.assertEqual(40, self.assault_rifle.fire_rate[0])
        self.assertEqual(120, self.assault_rifle.fire_rate[1])

    def test_shoot_fnscar_returns_correct_message(self):
        self.assertEqual('Shooting 120 rounds per minute!!!', self.assault_rifle.shoot())
