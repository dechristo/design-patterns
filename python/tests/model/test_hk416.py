from unittest import TestCase
from src.model.hk416 import HK416


class HK416Test(TestCase):
    def setUp(self):
        self.assault_rifle = HK416()

    def test_origin_is_Germany(self):
        self.assertEqual('Germany', self.assault_rifle.origin)

    def test_fire_rate_for_hk416_returns_correct_values(self):
        self.assertEqual(40, self.assault_rifle.fire_rate[0])
        self.assertEqual(100, self.assault_rifle.fire_rate[1])

    def test_shoot_hk416_returns_correct_message(self):
        self.assertEqual('Shooting 100 rounds per minute!!!', self.assault_rifle.shoot())
