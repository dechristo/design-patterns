from unittest import TestCase
from src.creational.factory.assault_rifle_factory import AssaultRifleFactory
from src.model.m16 import M16
from src.model.hk416 import HK416
from src.model.fnscar import FNSCAR


class AssaultRifleFactoryTest(TestCase):

    def test_create_M16_assault_rifle_returns_correct_object(self):
        assault_rifle = AssaultRifleFactory.create('M16')
        self.assertIsInstance(assault_rifle, M16)
        shoot = assault_rifle.shoot()
        self.assertEqual('Shooting 100 rounds per minute!!!', shoot)

    def test_create_FNSCAR_assault_rifle_returns_correct_object(self):
        assault_rifle = AssaultRifleFactory.create('FNSCAR')
        self.assertIsInstance(assault_rifle, FNSCAR)
        shoot = assault_rifle.shoot()
        self.assertEqual('Shooting 120 rounds per minute!!!', shoot)

    def test_create_HK416_assault_rifle_returns_correct_object(self):
        assault_rifle = AssaultRifleFactory.create('HK416')
        self.assertIsInstance(assault_rifle, HK416)
        shoot = assault_rifle.shoot()
        self.assertEqual('Shooting 100 rounds per minute!!!', shoot)

    def test_create_unimplemented_assault_rifle_returns_None(self):
        assault_rifle = AssaultRifleFactory.create('M4A1')
        self.assertIsNone(assault_rifle)
        assault_rifle = AssaultRifleFactory.create('AK47')
        self.assertIsNone(assault_rifle)

    def test_create_returns_None_if_type_equals_empty(self):
        assault_rifle = AssaultRifleFactory.create('')
        self.assertIsNone(assault_rifle)
