from src.model.fnscar import FNSCAR
from src.model.hk416 import HK416
from src.model.m16 import M16


class AssaultRifleFactory():

    @staticmethod
    def create(type):
        if type == 'HK416':
            return HK416()
        if type == 'FNSCAR':
            return FNSCAR()
        if type == 'M16':
            return M16()
        return None
