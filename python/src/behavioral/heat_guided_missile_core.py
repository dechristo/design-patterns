from src.model.heat_guided_missile_ogive import HeatGuidedMissileOgive
from src.model.heat_guided_missile_tracker import HeatGuidedMissileTracker

class HeatGuidedMissileCore:
    def __init__(self):
        self.tracker = HeatGuidedMissileTracker(self)
        self.ogive = HeatGuidedMissileOgive(self)

    def search_and_destroy(self):
        self.tracker.follow_heat(0,0,0,0)
        if self.tracker.getRemaingDistance() <= 1:
            self.ogive.detonate()
