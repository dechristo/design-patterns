class HK416:
    def __init__(self):
        self.origin = 'Germany'
        self.fire_rate = (40, 100)
        self.source = 'http://www.military-today.com/firearms/hk_416.htm'

    def shoot(self):
        return 'Shooting {0} rounds per minute!!!'.format(self.fire_rate[1])
