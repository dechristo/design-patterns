class FNSCAR:
    def __init__(self):
        self.origin = 'Belgium'
        self.fire_rate = (40, 120)
        self.source = 'http://www.military-today.com/firearms/fn_scar.htm'

    def shoot(self):
        return 'Shooting {0} rounds per minute!!!'.format(self.fire_rate[1])
