package structural.adapter;

import utils.FormattingTypes;

public class AdapterFormatterDemo {	
	
	public static void main(String[] args) {
		TextFormatterAdapter adapter = new TextFormatterAdapter();
		
		adapter.format("Ahoy There!", FormattingTypes.LOWER_CASE);
		adapter.format("this will work.", FormattingTypes.ALL_FIRST_UPPER);	
	}
}
