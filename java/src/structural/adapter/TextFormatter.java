package structural.adapter;

/**
 * 
 * @author Luiz Eduardo de Christo
 * 
 * Interface to simulate an old not so good implementation for a
 * string formatter to be replaced with a newer one through the 
 * use of an Adapter design pattern.
 *
 */
public interface TextFormatter {
	
	/**
	 * 
	 * @param String input
	 * @return String
	 */
	public String formatFirstLettersUpperCase(String input);
	
	/**
	 * 
	 * @param String input
	 * @return String
	 */
	public String formatUpperCase(String input);
	
	/**
	 * 
	 * @param String input
	 * @return String
	 */
	public String formatLowerCase(String input);
}
