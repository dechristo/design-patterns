package structural.adapter;

import utils.FormattingTypes;

public class TextFormatterAdapter implements GenericTextFormatter{

	private OldTextFormatter oldTextFormatter;
	
	public TextFormatterAdapter() {
		
	}
	
	@Override
	public String format(String str, FormattingTypes formatType) {
		String output = "Format type not implemented yet.";
		
		if(formatType.equals(FormattingTypes.ALL_FIRST_UPPER)) {
			output = this.oldTextFormatter.formatFirstLettersUpperCase(str);
		}
		
		return output;
	}
}
