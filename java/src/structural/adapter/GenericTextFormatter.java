package structural.adapter;

import utils.FormattingTypes;

public interface GenericTextFormatter {
	
	public String format(String str, FormattingTypes formaType);
}
