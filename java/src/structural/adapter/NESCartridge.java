package structural.adapter;

public interface NESCartridge {
	public boolean plug72Pin(String gameTitle);
	public boolean plug60Pin(String gameTitle);
}
