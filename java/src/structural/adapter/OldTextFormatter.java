package structural.adapter;

import java.util.ArrayList;
import java.util.List;

public class OldTextFormatter implements TextFormatter{
	
	@Override
	public String formatUpperCase(String data) {
		String[] current = data.split(" ");
		List<String> formatted = new ArrayList<String>(); 
		for(String word: current) {
			formatted.add(word.toUpperCase());
		}
		return String.join(" ", formatted);
	}

	@Override
	public String formatFirstLettersUpperCase(String input) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String formatLowerCase(String input) {
		// TODO Auto-generated method stub
		return null;
	}
}