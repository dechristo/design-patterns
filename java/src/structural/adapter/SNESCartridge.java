package structural.adapter;

public interface SNESCartridge {
	public boolean plug(String gameTitle);
}
