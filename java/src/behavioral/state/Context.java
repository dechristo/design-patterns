package behavioral.state;

import behavioral.state.zigzag.MovingRightState;

public abstract class Context {

	private BaseMovingState movingState;
	
	private String[][] matrix;
	
	private int l = 0;
	
	private int c = 0;
	
	private StringBuilder sb = new StringBuilder();
	
	private Context(String[][] matrix) {
		this.matrix = matrix;
		init();
	}
	
	public static Context build(String strategy, String[][] matrix) {
		if ("zigzag".equals(strategy)) {
			Context c = new Context(matrix) {
				@Override
				public boolean isGoalAchieved() {
					return isAtBottomBorder() && isAtRightBorder();
				}
				
				@Override
				public void init() {
					setMovingState(new MovingRightState());
				}
				
			};
			
			return c;
		}

		if ("linear".equals(strategy)) {
			Context c = new Context(matrix) {
				
				private int width;
				
				@Override
				public boolean isGoalAchieved() {
					return isAtBottomBorder() && (--width == 0);
				}
				
				@Override
				public void init() {
					width = matrix[0].length;
					setMovingState(new behavioral.state.linear.MovingRightState());
				}
			};
			
			return c;
		}
		
		return null;
	}
	
	public abstract boolean isGoalAchieved();
	
	public void init() {
		
	};

	public String traverse() {
		while (!isGoalAchieved()) {
			movingState.move(this);
		}
		movingState.move(this);
		System.out.println(sb);
		return sb.toString();
	}
	
	public void setMovingState(BaseMovingState maxItState) {
		this.movingState = maxItState;
	}
	
	public final boolean isAtLeftBorder() {
		return c == 0;
	}
	
	public final boolean isAtRightBorder() {
		return c == matrix[0].length - 1;
	}
	
	public final boolean isAtTopBorder() {
		return l == 0;
	}
	
	public final boolean isAtBottomBorder() {
		return l == matrix.length - 1;
	}

	public void moveRight() {
		visit();
		c++;
	}
	
	public void moveLeft() {
		visit();
		c--;
	}
	
	public void moveUp() {
		visit();
		l--;
	}
	
	public void moveDown() {
		visit();
		l++;
	}

	public void moveSW() {
		visit();
		l++;
		c--;
	}

	public void moveNE() {
		visit();
		l--;
		c++;
	}

	protected void visit() {
		System.out.println(matrix[l][c]);
		sb.append(matrix[l][c]).append(" ");
	}
	
}
