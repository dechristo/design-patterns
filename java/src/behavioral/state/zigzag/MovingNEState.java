package behavioral.state.zigzag;

import behavioral.state.BaseMovingState;
import behavioral.state.Context;

public class MovingNEState extends BaseMovingState {

	@Override
	protected void move(Context ctx) {
		ctx.moveNE();
		if (ctx.isAtRightBorder())
			ctx.setMovingState(new MovingDownState());
		else if (ctx.isAtTopBorder())
			ctx.setMovingState(new MovingRightState());
	}

}
