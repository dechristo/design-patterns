package behavioral.state.zigzag;

import behavioral.state.BaseMovingState;
import behavioral.state.Context;

public class MovingRightState extends BaseMovingState {

	@Override
	protected void move(Context ctx) {
		ctx.moveRight();
		if (ctx.isAtTopBorder())
			ctx.setMovingState(new MovingSWState());
		else
			ctx.setMovingState(new MovingNEState());
	}

}
