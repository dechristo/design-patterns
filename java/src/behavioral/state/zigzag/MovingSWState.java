package behavioral.state.zigzag;

import behavioral.state.BaseMovingState;
import behavioral.state.Context;

public class MovingSWState extends BaseMovingState {

	@Override
	protected void move(Context ctx) {
		ctx.moveSW();
		if (ctx.isAtBottomBorder())
			ctx.setMovingState(new MovingRightState());
		else if (ctx.isAtLeftBorder())
			ctx.setMovingState(new MovingDownState());
	}

}
