package behavioral.state.zigzag;

import behavioral.state.BaseMovingState;
import behavioral.state.Context;

public class MovingDownState extends BaseMovingState {

	@Override
	protected void move(Context ctx) {
		ctx.moveDown();
		if (ctx.isAtLeftBorder())
			ctx.setMovingState(new MovingNEState());
		else
			ctx.setMovingState(new MovingSWState());
	}

}
