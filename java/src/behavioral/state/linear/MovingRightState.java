package behavioral.state.linear;

import behavioral.state.BaseMovingState;
import behavioral.state.Context;

public class MovingRightState extends BaseMovingState {

	@Override
	protected void move(Context ctx) {
		ctx.moveRight();
		if (ctx.isAtRightBorder())
			ctx.setMovingState(new MovingDownState());
	}

}
