package behavioral.state.linear;

import behavioral.state.BaseMovingState;
import behavioral.state.Context;

public class MovingDownState extends BaseMovingState {

	@Override
	protected void move(Context ctx) {
		ctx.moveDown();
		if (ctx.isAtRightBorder())
			ctx.setMovingState(new MovingLeftState());
		else
			ctx.setMovingState(new MovingRightState());
	}

}
