package behavioral.state.linear;

import behavioral.state.BaseMovingState;
import behavioral.state.Context;

public class MovingLeftState extends BaseMovingState {

	@Override
	protected void move(Context ctx) {
		ctx.moveLeft();
		if (ctx.isAtLeftBorder())
			ctx.setMovingState(new MovingDownState());
	}

}
