package behavioral.state;

public abstract class BaseMovingState {

	protected abstract void move(Context ctx);
	
}
