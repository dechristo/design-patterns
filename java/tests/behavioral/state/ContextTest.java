package behavioral.state;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ContextTest {

	private String[][] matrix;
	
	@Before
	public void setUp() throws Exception {
		matrix = new String[][] {
			new String[] {"00", "01", "02", "03", "04"},
			new String[] {"10", "11", "12", "13", "14"},
			new String[] {"20", "21", "22", "23", "24"},
			new String[] {"30", "31", "32", "33", "34"},
		};
	}

	@Test
	public void testZigzag() {
		Context context = Context.build("zigzag", matrix);
		assertEquals("00 01 10 20 11 02 03 12 21 30 31 22 13 04 14 23 32 33 24 34 ",
				context.traverse());
	}

	@Test
	public void testLinear() {
		Context context = Context.build("linear", matrix);
		assertEquals("00 01 02 03 04 14 13 12 11 10 20 21 22 23 24 34 33 32 31 30 ",
				context.traverse());
	}
}
